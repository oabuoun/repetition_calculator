import json, bitarray, itertools, sys, os.path, operator
import pandas as pd
from itertools import groupby
from io import StringIO
import matplotlib.pyplot as plt
from pprint import pprint
from collections import Counter
import dask
import dask.dataframe as dd

class Repetition:
	def __init__(self, file, length):
		self.file = file
		self.length = int(length)
		self.output_path = "./results"
		self.result_file_name = "{}/{}".format(self.output_path, "repetition-{}-bytes.{}")
		self.result_file_name_byte_position_based = "{}/{}".format(self.output_path, "repetition-{}-bytes-position-based.{}")

	def run(self):
		print("+++ Data File: {}".format(self.file))

		# Create output folder
		self.mkdirs(self.output_path)

		# Read the data as bits and converts them to bytes
		self.bytearray_data = self.read_binary_bits(self.file).tobytes()
		print("+++ {}: {} Bytes".format(self.file, len(self.bytearray_data)))

		# Get header_columns as list for processing and as a text to write it directly to a csv file
		header_columns, header_columns_text = self.get_header_columns()

		# Get a list of chunks (Each has {self.length} bytes)
		chunks = self.get_chunks()

		# Generate a list (and write it to a file) of Unique_chunks
		unique_values = self.get_unique_value_list(chunks)

		# Generate a csv file/text which contains the every unique chunk and how many times it was repeated
		#repetition_result_csv = self.count_repetitions(chunks, unique_values, header_columns_text) # This line uses the slow counting method
		repetition_result_csv = self.count_repetitions_fast_method(chunks, header_columns_text) # This line uses the fast counting method using {Counter} Library

		# Process the CSV using Pandas Linrary, this line load the CSV into {Pandas DataFrame} according to the columns passed in the first argument
		df = self.get_df(header_columns, repetition_result_csv)

		# Generate the summary of repetition results and write it CSV file
		self.generate_summary(df)

		# Find the Unique Values in Each Byte of the chunks : 1st-Byte, 2nd-Byte, ..etc
		self.process_unique_ascii_per_byte(df)

		df = df.sort_values(by=['Count'], ascending=False)

		result_file_csv = self.result_file_name.format(self.length, "csv")
		df.to_csv(result_file_csv, index = True, header=True)

		# ASCII-Based Search: find out which ASCII-code appreared in which Byte and how many times
		self.calc_position_based(df, header_columns)

		# Generate plots for all the results
		self.generate_plots()

	def get_chunks(self):
		# Put the data into chunks of {self.length} bytes
		chunks = [self.bytearray_data[x:x+self.length] for x in range(0, len(self.bytearray_data), self.length)]
		# Delete the last chunk if its length is less than the needed one (self.length)
		if len(chunks[-1]) < self.length:
			chunks = chunks[:-1]
		print("++ Chunks: {} chunk of {} bytes each".format(len(chunks), self.length))
		return chunks

	def get_header_columns(self):
		column = "Byte-{},"
		text = "".join([column.format(i) for i in range(1, self.length + 1)]) + "Count" + "\n"
		header_columns = text.rstrip().split(",")
		return header_columns, text

	def get_unique_value_list(self, chunks):
		# Generate a list of all unique chunks
		unique_values = [s for s in set([chunk for chunk in chunks])]
		pprint(unique_values)
		print("++ Unique_values: {} unique values".format(len(unique_values)))
		return unique_values

	def count_repetitions(self, chunks, unique_values, header_columns_text):
		# Unique value count
		unique_values_count = len(unique_values)

		index = 0
		csv = header_columns_text + ""

		# Loop over the unique values
		for unique_value in unique_values:
			index += 1
			print("Unique Value {}/{} = {}".format(index, unique_values_count, unique_value), end="\r")

			# find out how many times this value was repeated
			count = chunks.count(unique_value)

			# convert it from string to byte list separated by commas
			asBytes = ",".join([str(b) for b in unique_value])
			printable = "{},{}".format(asBytes, count)

			# Add the printable form to the CSV file
			csv += printable + "\n"
			print("hex=({}), \t\tbytes=({}), \t\tcount={}".format(unique_value, asBytes, count))
		csv = csv.rstrip()
		return csv

	def count_repetitions_fast_method(self, chunks, header_columns_text):
		# Similar to count_repetitions except that it is much faster than the other one
		csv = header_columns_text + ""
		repetitions = dict(Counter(chunks))
		for key, count in repetitions.items():
			key_bytes = ",".join([str(b) for b in key])
			printable = "{},{}".format(key_bytes, count)
			csv += printable + "\n"
		csv = csv.rstrip()
		return csv

	def process_unique_ascii_per_byte(self, df):
		# Reset the index if exists and drop the Count columns
		df1 = df.reset_index()
		df1 = df1.drop("Count", axis=1)
		unique_ascii_per_byte = {}
		max_len = 0

		# Loop through the columns (1st byte, 2nd byte, ...etc) in the DataFrame
		for column in list(df1.columns):
			# Get a list of unique ascii code in this column in sort the value (Ascending)
			unique_ascii_per_byte[column] = sorted(list(df1[column].unique()))
			max = len(unique_ascii_per_byte[column])
			# Find out the max value in order to fill the rest of values with 0's
			max_len = max if max > max_len else max_len

		# Generate summary file
		summary_header = "Byte-"
		summary_count = "Unique ASCII"
		index = 1
		for column in unique_ascii_per_byte.keys():
			values = unique_ascii_per_byte[column]

			summary_header = "{},{}".format(summary_header, index)
			summary_count  = "{},{}".format(summary_count, len(values))
			index += 1
			for value in range(len(values), max_len):
				unique_ascii_per_byte[column].append("")

		# Generate a DataFrame based on the dictionary
		unique_data_df = pd.DataFrame.from_dict(unique_ascii_per_byte)

		# Write it as CSV file
		result_file_csv = self.result_file_name.format(self.length, "-unique-values.csv")
		unique_data_df.to_csv (result_file_csv, index = False, header=True)

		summary = "{}\n{}".format(summary_header, summary_count)
		result_file_csv = self.result_file_name.format(self.length, "-unique-values-summary.csv")
		self.write(result_file_csv, summary)
		return unique_ascii_per_byte

	def generate_summary(self, df):
		# Convert Pandas DataFrame to Dask DataFrame
		df = dd.from_pandas(df, npartitions= self.length)

		# Calculate the sum of all repetitions (it should be equal to number of chunks)
		total_data_values = df['Count'].sum().compute()

		# Add new column that contains the percent of repetition for each chunk
		percent = df['Count']/total_data_values*100
		df['Percent'] = percent

		# Calculate rows count (it should be equal to number of unique values)
		total_row_count = len(df.index)

		# Sort the Data - Descending order of the percent
		df = df.nlargest(total_row_count, 'Percent').compute()

		# This is the header of the CSV file
		text = "Unique Series,Unique Series % Total Data,% Unique Series,Repetition,Repetition%,Total Data,Percentage\n"

		# What percentages should be calculated
		percentages = [10, 20 , 30, 40, 50, 60, 70, 80, 90]

		# Current Percent - initial 0
		percent_sum = 0

		# How many row should be taken into sum
		row_count = 1

		# A list to save the results (how many rows, count and real percentages) of each round
		percentage_row_counts = []
		print("percentages={}".format(percentages))

		# Go through percentages one by one
		# Keep adding rows and calculate the sum of percents until it's the nearest to the needed percentage
		for percentage in percentages:

			print("percentage={}, total_row_count={}, percent_sum={}, row_count={}, percentage_row_counts={}".format(percentage, total_row_count, percent_sum, row_count, percentage_row_counts))

			# Difference between the needed percentage and the current percentage
			difference = percentage

			# Save a list of all percentages found, this helps to prevent getting stuck in an infinite loop
			percentage_records = []

			# True when it should stops because we have the nearest possible percentage
			finished = False

			# Define whether rows should be added/removed to go up/down to the needed percentage
			#	True: 	(current percentage < needed percentage) -> Add more rows
			#	False: 	(current percentage > needed percentage) -> Remove from the rows
			increasing = True

			# A variable to save the point which should be the minimum number of rows
			min_point = 1
			while not finished:
				print("difference={}, finished={}, increasing={}, percentage_records={}".format(difference, finished, increasing, percentage_records))

				# Calculate the sum of row_count rows
				percent_sum = df.head(row_count)['Percent'].sum()

				# Calculate the difference between the needed percentage and the current percentage
				difference = abs(percentage - percent_sum)

				# Add it to percentage_records to prevent infinite loop
				percentage_records.append({'row_count': row_count, 'difference': difference })
				print("row_count={}, {}==?{} = {}".format(row_count, percentage, percent_sum, percent_sum == percentage))

				# current percentage is equal to the needed one
				if percent_sum == percentage:
					# while loop is finished
					finished = True
					# Add the current row count to the final list
					percentage_row_counts.append(row_count)
					print("finished={}, row_count={}".format(finished, row_count))
				elif percent_sum > percentage and increasing:
					# current percentage > needed percentage and it is still in increasing mode
					print("len(percentage_records)={}".format(len(percentage_records)))
					if len(percentage_records) > 1:
						# There are more than one result, so it is possible to find another percentage with smaller difference
						min_point = percentage_records[-2]['row_count']
						increasing = False
						print("min_point={}, increasing={}".format(min_point, increasing))
					else:
						# There is only one result in the list which means that it is impossible to go lower than this percentage
						finished = True
						percentage_row_counts.append(row_count)
						print("finished={}, row_count={}".format(finished, row_count))
						continue

				print("finished={}, increasing={}, min_point={}, percentage_records={}".format(finished, increasing, min_point, percentage_records))

				if increasing:
					# in increasing mode, double the current row_count and continue
					row_count = row_count * 2
					print("row_count={}".format(row_count))
				else:
					# in decreasing mode
					print("percent_sum= {} , percentage= {}".format(percent_sum, percentage))
					if percent_sum > percentage:
						# current percentage is higher than the needed one, so consider this as the max_point
						max_point = row_count
						print("percent_sum > percentage, max_point= {}".format(max_point))
					elif percent_sum < percentage:
						# current percentage is smaller than the needed one, so consider this as the min_point
						min_point = row_count
						print("percent_sum < percentage, min_point= {}".format(min_point))

					# find the point (row_count) in the middle between min_point and max_point
					row_count = min_point + int((max_point - min_point)/2)
					print("row_count={}".format(row_count))

					# Check if current row_count has been calculated before and stop if so
					if row_count in map(operator.itemgetter('row_count'), percentage_records):
						print("row_count in percentage_records")

						finished = True

						# Find the result with the smallest difference
						best_record = min(percentage_records, key=lambda x:x['difference'])
						row_count = best_record['row_count']
						percentage_row_counts.append(row_count)

						print("percentage: {}, percent_sum: {}, record:{}".format(percentage, percent_sum, best_record))
						continue
					print("row_count NOT in percentage_records")

			# Add the result to the CSV text
			text += "{},{} ({:.4f}%),{}/{} ({:.4f}%),{:.0f},{:.0f} ({:.2f}%), {}, {:.2f}% (\u2243 {}%)\n".format(row_count, row_count, row_count/total_row_count*100, row_count, total_row_count, row_count/total_row_count*100, total_data_values*percent_sum/100, total_data_values*percent_sum/100, row_count/total_row_count*100, total_data_values, percent_sum, percentage)

		# Add the final row in the CSV text (100%)
		text += "{},{} ({:.4f}%),{}/{} ({:.4f}%),{:.0f},{:.0f} ({:.2f}%), {}, {:.2f}% (\u2243 {}%)\n".format(total_row_count, total_row_count, 100, total_row_count, total_row_count, 100, total_data_values, total_data_values, 100, total_data_values, 100, 100)
		txt_file_name = self.result_file_name.format(self.length, "-summary.csv")
		print(text)
		self.write(txt_file_name, text)


	def calc_position_based(self, df, header_columns):
		# Reset the index of the DataFrame
		df = df.reset_index()

		data = {}
		# Loop throug the columns in the DataFrame
		for col in header_columns[:-1]:
			# Get a list of values in this column
			values = df[col].tolist()
			column_data = {}

			# Loop throug the values in the column
			for value in values:

				if value not in column_data:
					# a new value appeared, add it to the list but with count of 0
					column_data[value] = 0

				# Add +1 to mark that this value appeared one more time
				column_data[value] += 1
				print("column_data[{}] = {}".format(value, column_data[value]))
			# Check the missing values in this list and initialize them to 0
			data[col] = self.fill_missing_bytes(column_data)

		pprint(data)
		# Generate DataFrame from based on the dictionary
		data_df = pd.DataFrame.from_dict(data)
		# Define the index to be the first column which ASCII code
		data_df.index.name = "Ascii"
		# Sort the values
		data_df = data_df.sort_values(by=['Ascii'], ascending=True)
		# Write the results in a CSV file
		result_file_name_byte_position_based = self.result_file_name_byte_position_based.format(self.length, "csv")
		data_df.to_csv (result_file_name_byte_position_based, index = True, header=True)

	def get_csv_text(self, repetition_list):
		column = "Byte-{},"
		text = "".join([column.format(i) for i in range(1, self.length + 1)]) + "Count" + "\n"
		for bytes, count in repetition_list.items():
			text += "".join([str(b) + "," for b in bytes]) + str(count) + "\n"
		return text

	def fill_missing_bytes(self, d):
		for byte in range(0, 256):
			if not byte in d:
				d[byte] = 0
		return d

	def get_df(self, header_columns, text):
		StringData = StringIO(text)
		csv = pd.read_csv(StringData, sep =",", index_col= header_columns[:1])
		pprint(csv)
		return pd.DataFrame(csv)

	def generate_plots(self):
		result_file_name_byte_position_based = self.result_file_name_byte_position_based.format(self.length, "csv")
		generated = os.path.exists(result_file_name_byte_position_based)
		if generated:
			t_df = pd.read_csv(result_file_name_byte_position_based, sep =",", index_col="Ascii", dtype={'Ascii': 'Int64'}).sort_values(by=['Ascii'], ascending=True)

			result_file_png = self.result_file_name.format(self.length, "png")
			self.plot(t_df, "Byte Repetition - Based on Ascii Value, {}".format(self.length), result_file_png)

			result_file_png = self.result_file_name_byte_position_based.format(self.length, "png")
			self.plot(t_df, "Byte Repetition - Based on Byte Position, {}".format(self.length), result_file_png, reversed = True)

	def plot(self, df, subtitle, output_png, reversed = False):
		if not df.empty:
			plt.rcParams["figure.figsize"] = [36,36]
			#pprint(df)
			ticks = [x for x in range(0, 256, 1)] #sample names
			if reversed:
				df.T.plot(kind="bar")
				plt.xlabel("Bytes")
			else:
				plt.xlabel("Ascii Value")
				df.plot(kind="bar")
				plt.xticks(ticks, fontsize=4, rotation=65)
			#plt.suptitle(subtitle)
			plt.ylabel('Count')
			plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=20, mode="expand", borderaxespad=0.)
			plt.savefig(output_png, dpi = 300)
			plt.close('all')
			plt.clf()

	def read_binary_bits(self, path):
		data = bitarray.bitarray()
		with open(path, 'rb') as binaryfile:
			data.fromfile(binaryfile)
		return data

	def write(self, path, text, append = False):
		mode = "a" if append else "w"
		f = open(path, mode)
		f.write(text)
		f.close()

	def mkdirs(self, path):
		try:
			os.makedirs(path)
		except Exception as e:
			print("++ " + str(e))
			pass

if __name__ == '__main__':
	args = len(sys.argv)
	print(sys.argv)
	if args < 3:
		print("Usage: \n\tpython3 repetition data_file_path chunk_length")
	else:
		data_file = sys.argv[1]
		chunk_length = sys.argv[2]

		repetition = Repetition(data_file, chunk_length)
		repetition.run()
